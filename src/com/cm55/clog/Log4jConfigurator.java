package com.cm55.clog;

import java.io.*;
import java.util.*;
import java.util.stream.*;

import org.apache.log4j.*;

/**
 * Log4jのコンフィグレーションを行う。
 */
class Log4jConfigurator {
  
  /** 現在のコンフィギュレーション。されていない場合はnull */
  private static String configuration = null;  
  
  /** 
   * 文字列を与えて設定をリセットする。
   * ※ひどいことにLog4jは指定されたプロパティにエラーのある場合に例外を発生しない。
   * そうではなく、標準エラー出力にエラー文字列を出力するだけ。これは内部的にLogLog.errorで行われている。
   * これではうっかり設定したつもりで設定されていない場合がありうるので例外を発生するようにしてある。
   * @param newConfig
   * @throws Log4jException
   */
  synchronized static void setConfiguration(String newConfig) throws Log4jException {
    
    // 文字列をプロパティに変換。ここでエラー発生の可能性あり
    Properties props = stringToProperties(newConfig);
    
    // 既にコンフィグされている場合はいったんリセット
    if (configuration != null) LogManager.resetConfiguration();
    
    // 標準エラー出力を一時的に奪い取り、文字列として取得する
    String err;
    try (ErrReplacer replacer = new ErrReplacer()) {
      try {
        PropertyConfigurator.configure(props);
      } catch (NoClassDefFoundError ex) {
        // Log4jライブラリがクラスパスに無い場合にこのエラーが発生する。単純に無視する。
        return;
      }
      err = replacer.get();
    }
    System.err.println(err);
    analyzeError(err);
    
    // 設定OK
    configuration = newConfig;
  }

  private static final String E = "log4j:ERROR ";
  
  private static void analyzeError(String errorOutput) throws Log4jException {
    List<String>list = Arrays.stream(errorOutput.split("[\n\r]"))
      .filter(s->s.length() > 0)
      .map(s->s.startsWith(E)? s.substring(E.length()):s)
      .collect(Collectors.toList());
    if (list.size() == 0) return;
    if (list.size() == 1) throw new Log4jException(list.get(0));
    throw new Log4jException(list.get(0) + " (and others)");
  }
  
  /** 現在のコンフィギュレーションを取得する。未設定の場合はnull */
  static synchronized String getConfiguration() {
    return configuration;
  }

  /** 改行まじりの設定文字列をPropertiesに変換。プロパティ形式違反でエラー発生 */
  static Properties stringToProperties(String s) throws Log4jException {
    Properties props = new Properties();
    try {
      props.load(new StringReader(s));
    } catch (Exception ex) {
      throw new Log4jException(ex.getMessage());
    }
    return props;
  }
}
