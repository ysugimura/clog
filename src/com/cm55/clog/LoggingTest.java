package com.cm55.clog;

import static org.junit.Assert.*;

import org.apache.commons.logging.*;
import org.apache.log4j.spi.*;
import org.junit.*;

public class LoggingTest {
  
  static Log fooLogger;
  static Log barLogger;
  static Log foobarLogger;

  @Test
  public void test() throws Exception {
    
    fooLogger = LogFactory.getLog("com.sample.foo");
    barLogger = LogFactory.getLog("com.sample.bar");
    foobarLogger = LogFactory.getLog("com.foobar");

    CLogFactory.setConfiguration(
      "log4j.rootLogger=fatal,CONSOLE\n" +
      "log4j.appender.CONSOLE=" + StringAppender.class.getName() + "\n" +
      //"log4j.appender.CONSOLE=org.apache.log4j.ConsoleAppender\n" +
      "log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout\n" +
      "log4j.appender.CONSOLE.layout.ConversionPattern=%-5p %c{1} - %m%n\n" +
      "log4j.logger.com.sample=info\n" +
      "log4j.logger.com.sample.foo=trace"
    );

    logSomething();
    
    CLogFactory.setConfiguration(
      "log4j.rootLogger=trace,CONSOLE\n" +
      "log4j.appender.CONSOLE="  + StringAppender.class.getName() + "\n" +
      //"log4j.appender.CONSOLE=org.apache.log4j.ConsoleAppender\n" +
      "log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout\n" +
      "log4j.appender.CONSOLE.layout.ConversionPattern=%-5p %c{1} - %m%n\n" +
      "log4j.logger.com.sample=fatal\n" +
      "log4j.logger.com.sample.foo=info"
    );

    logSomething();
    
    assertEquals(
        "FATAL foo - fatal\n" + 
        "INFO  foo - info\n" + 
        "DEBUG foo - trace\n" + 
        "FATAL bar - fatal\n" + 
        "INFO  bar - info\n" + 
        "FATAL foobar - fatal\n" + 
        "FATAL foo - fatal\n" + 
        "INFO  foo - info\n" + 
        "FATAL bar - fatal\n" + 
        "FATAL foobar - fatal\n" + 
        "INFO  foobar - info\n" + 
        "DEBUG foobar - trace\n" + 
        "", StringAppender.s.toString());
        
  }
  
  static void logSomething() {
    fooLogger.fatal("fatal");
    fooLogger.info("info");
    fooLogger.trace("trace");
    
    barLogger.fatal("fatal");
    barLogger.info("info");
    barLogger.trace("trace");
    
    foobarLogger.fatal("fatal");
    foobarLogger.info("info");
    foobarLogger.trace("trace");    
  }

  public static class StringAppender extends AbstractLogEventAppender {
    static StringBuilder s = new StringBuilder();
    protected void dispatchEvent(LoggingEvent log4jEvent, LogEvent e) {

      s.append(e.message + "\n");
    }    
  }
}
