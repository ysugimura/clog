package com.cm55.clog;

import java.io.*;

/**
 * ログイベント
 */
class LogEvent implements Serializable {
  
  public final String message;
  public final String[]th;
  
  LogEvent(String message, String[]th) {
    this.message = message;
    this.th = th;
  }
  
  @Override
  public String toString() {
    return "LogEvent " + message + ", " + th;
  }
}
