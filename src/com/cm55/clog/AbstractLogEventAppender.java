package com.cm55.clog;


import org.apache.log4j.*;
import org.apache.log4j.spi.*;



/**
 * log4jログを内部イベントとしてディスパッチするためのアペンダ
 * <p>
 * Log4jのログ設定における途中の改行はすべて"\n"になる。
 * 最後の"\n"は削除されていることに注意
 * </p>
 */
 abstract class AbstractLogEventAppender extends AppenderSkeleton  {

  private static boolean DEBUG = false;
  
  /** log4jのイベントを内部イベントに変換してディスパッチする */
  @Override
  protected void append(LoggingEvent event) { 
    String formatted = this.layout.format(event);    
    formatted = formatted.replace("\r\n",  "\n");
    if (formatted.endsWith("\n")) formatted = formatted.substring(0, formatted.length() - 1);
    
    if (DEBUG) {
      // パターン中の"%n"によって変換された文字列には改行が付加される。
      // Windowsの場合は"\r\n"になってしまう。
      // ログをクライアントからサーバに送り込む場合にはサーバ側で除去すること。
      System.out.println("formatted[" + formatted + "] by " + layout.getClass());
      int len = formatted.length();
      System.out.println("" + (int)formatted.charAt(len - 2) + ", " + (int)formatted.charAt(len - 1));
    }
    
    LogEvent e =  new LogEvent(formatted, event.getThrowableStrRep());
    
    if (DEBUG) {
      System.out.println("dispatchEvent " + e);
    }
    dispatchEvent(event,e);    
  }

  /** レイアウトが必要 */
  public boolean requiresLayout() {
    return true;    
  }
  
  public final void close() {}
  
  protected abstract void dispatchEvent(LoggingEvent log4jEvent, LogEvent e);
}
