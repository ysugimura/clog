package com.cm55.clog;

import java.util.function.*;

import org.apache.commons.logging.*;

public class CLog {
  
  /** commons-loggingオブジェクト。commons-loggingライブラリがロードされていないときにはnullが格納されていることに注意 */
  private Log log;

  /** commons-loggingオブジェクト、あるいはnullが指定される */
  CLog(Log log) {
    this.log = log;
  }
  
  public Log getLog() {
    return log;
  }
  
  /** commons-loggingオブジェクト、あるいはnullが指定される */
  void set(Log log) {
    this.log = log;    
  }
  
  public boolean ist() {
    if (log == null) return false;
    return log.isTraceEnabled();
  }
  public void trace(String s) {
    if (log != null)
    log.trace(s);
  }
  
  public void trace(String s, Throwable th) {
    if (log != null)
    log.trace(s, ThrowUtil.getOriginCause(th));
  }
  
  public void info(String s) {
    if (log != null)
    log.info(s);
  }
  
  public void warn(String s) {
    if (log != null)
    log.warn(s);
  }
  
  public void warn(String s, Throwable th) {
    if (log != null)
    log.warn(s, ThrowUtil.getOriginCause(th));
  }
  
  public void error(String s) {
    if (log != null)
    log.error(s);
  }
  
  public void error(String s, Throwable th) {
    if (log != null)
    log.error(s, ThrowUtil.getOriginCause(th));
  }
  
  public void fatal(String s) {
    if (log != null)
    log.fatal(s);
  }
  
  public void fatal(String s, Throwable th) {
    if (log != null)
    log.fatal(s, ThrowUtil.getOriginCause(th));
  }

  public void t(Supplier<String> function) {
    if (!ist()) return;
    if (log != null)
    trace(function.get());
  }
}
