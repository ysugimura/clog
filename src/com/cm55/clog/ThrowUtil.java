package com.cm55.clog;

class ThrowUtil {

  /**
   * ある例外の最も根源の例外を取得する
   * @param th
   * @return
   */
  static Throwable getOriginCause(Throwable th) {
    while (true) {
      Throwable cause = th.getCause();
      if (cause == null) break;
      th = cause;
    }
    return th;
  }
}
