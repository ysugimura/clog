package com.cm55.clog;

import java.util.*;
import java.util.stream.*;

import org.apache.commons.logging.*;

/** ログファクトリ */
public class CLogFactory {

  /** デフォルトとして設定する子フィギュレーション。エラーが発生しないこと */
  public static final String DEFAULT_CONFIG =
    "log4j.rootLogger=info,CONSOLE\n" +
    "log4j.appender.CONSOLE=org.apache.log4j.ConsoleAppender\n" +
    "log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout\n" +
    "log4j.appender.CONSOLE.layout.ConversionPattern=%d{MM/dd HH:mm} %-5p %c{1} - %m%n\n";
  
  /**
   * 初期化を行う。
   * commons-logにLog4jを使わせるようにし、Log4ｊのデフォルトコンフィグをする。
   */
  static {
    CommonsLoggingConfigurator.config();
    try {
      Log4jConfigurator.setConfiguration(DEFAULT_CONFIG);
    } catch (Exception ex) {
      // 起こりえない
      ex.printStackTrace();
    }
  }

  /** クラスに対するロガーを取得する */
  public static CLog getLog(Class<?>clazz) {
    return get(clazz.getName(), true);
  }

  /** 名称に対するロガーを取得する。おそらくこれが使われることは無い。 */
  public static CLog getLog(String name) {
    return get(name, false);
  }

  /** CLogを作成する。クラス名から作成したものについては特殊な処理をする */
  private synchronized static CLog get(String name, boolean clazz) {
    CLog log = new CLog(createCommonsLogging(name));    
    if (clazz) classNameMap.put(name, log);
    return log;
  }
  
  /** Log4jコンフィグを初期状態に戻す */
  public static void resetConfiguration() {
    try {
      Log4jConfigurator.setConfiguration(DEFAULT_CONFIG);
    } catch (Exception ex) {
      // 起こりえない
      throw new InternalError(ex);
    }
  }
  
  /** 現在のLog4jコンフィグを取得する */
  public static String getConfiguration() {
    return Log4jConfigurator.getConfiguration();
  }

  /** Log4jコンフィグを設定する */
  public static void setConfiguration(String s) throws Log4jException {
    Log4jConfigurator.setConfiguration(s);
  }
  
  private static Log createCommonsLogging(String name) {
    try {
      return LogFactory.getLog(name);
    } catch (NoClassDefFoundError ex) {
      // commons-loggingがロードされていない場合にこのエラーになる。単純に無視してnullを返す。
      return null;
    }
  }

  /* クラス名称/CLogマップ =================== */
  
  /* クラス名称/CLogマップ */
  private static Map<String, CLog>classNameMap = new HashMap<>();
  
  /**
   * CLogの名称を変更する。
   * 引数として旧名称/新名称のマップを与えると、すべてのクラスから作成された旧名称ロガーを新名称に変更する。
   * @param replaceMap
   */
  public static void replaceClassNames(Map<String, String>replaceMap) {
    replaceMap.entrySet().stream().forEach(e-> {
      CLog log = classNameMap.get(e.getKey());
      if (log != null) log.set(createCommonsLogging(e.getValue()));
    });
  }
  
  
  /* 以下はユニットテスト用 =================== */
  
  /** 
   * 指定クラスのロガーのトレースを出力するように現在のLog4j設定に追加する。これはユニットテスト用。
   * ユニットテストの際に、対象クラスを指定することにより簡単にトレースログを出力する。
   */
  public static void trace(Class<?>...classes) {
    trace(
      Arrays.stream(classes).map(c->c.getName()).collect(Collectors.toList()).toArray(new String[0])
    );
  }
  
  /** 指定名称のロガーのトレースを出力するように現在のLog4j設定に追加する。これはユニットテスト用。おそらくこれが直接使われることは無い。 */
  public static void trace(String...names) {
    StringBuilder config = new StringBuilder(getConfiguration());
    Arrays.stream(names).forEach(s->config.append("\n" + "log4j.logger." + s + "=trace"));
    try {
    setConfiguration(config.toString());
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }
}
