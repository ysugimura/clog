package com.cm55.clog;

import java.io.*;
import java.nio.charset.*;

/**
 * 標準エラー出力を一時的にリダイレクトし、文字列として取得する
 * @author ysugimura
 */
public class ErrReplacer implements Closeable {
  
  PrintStream savedStream;
  StringPrintStream replacedStream;
  
  public ErrReplacer() {
    savedStream = System.err;
    replacedStream = new StringPrintStream();
    System.setErr(replacedStream.ps);
  }
  
  @Override
  public void close() {
    if (savedStream == null) throw new RuntimeException("already closed");
    System.setErr(savedStream);
    savedStream = null;
  }
  
  public String get() {
    return replacedStream.get();
  }
  
  public static class StringPrintStream  {
    static final Charset charset =  Charset.forName(System.getProperty("file.encoding"));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    PrintStream ps;
    public StringPrintStream() {
      baos  = new ByteArrayOutputStream();
      try {
        ps = new PrintStream(baos, true, charset.name());
      } catch(Exception ex) {
        throw new RuntimeException(ex);
      }
    }
    public void close() {
      ps.close();
    }
    public String get() {
      return new String(baos.toByteArray(), charset);
    }
  }
}
