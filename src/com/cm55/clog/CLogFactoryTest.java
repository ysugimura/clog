package com.cm55.clog;

import static org.junit.Assert.*;

import org.apache.log4j.spi.*;
import org.junit.*;


public class CLogFactoryTest {

  static StringBuilder logPool = new StringBuilder();
  
  @Before
  public void before() throws Log4jException {
    CLogFactory.setConfiguration(
      "log4j.rootLogger=fatal,CONSOLE\n" +
      "log4j.appender.CONSOLE=" + StringAppender.class.getName() + "\n" +
      "log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout\n" +
      "log4j.appender.CONSOLE.layout.ConversionPattern=%-5p %c{1} - %m%n\n"
    );
    logPool = new StringBuilder();    
  }
  
  @Test
  public void test() {    
    CLog fooLogger = CLogFactory.getLog("foo");
    CLogFactory.trace("foo");    
    fooLogger.trace("test");

    assertEquals("DEBUG foo - test\n", logPool.toString());
  }

  public static class StringAppender extends AbstractLogEventAppender {
    protected void dispatchEvent(LoggingEvent log4jEvent, LogEvent e) {
      logPool.append(e.message + "\n");
    }    
  }
}
