package com.cm55.clog;

/**
 * commons-log + Log4ｊを使うためのコンフィギュレーション
 * <p>
 * システムプロパティにLog4Jの使用を指示すればよい。
 * </p>
 * @author ysugimura
 */
class CommonsLoggingConfigurator {

  static final String COMMONS_LOGGING_LOG = "org.apache.commons.logging.log";
  static final String LOGGING_IMPL_LOG4JLOGGER = "org.apache.commons.logging.impl.Log4JLogger";
  
  static void config() {
    System.setProperty(COMMONS_LOGGING_LOG, LOGGING_IMPL_LOG4JLOGGER);    
  }
}
