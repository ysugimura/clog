package com.cm55.clog;

public class Log4jException extends Exception {

  public Log4jException(String message) {
    super(message);
  }

}
